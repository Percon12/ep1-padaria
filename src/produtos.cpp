#include "produtos.hpp"
#include <string>
#include <fstream>
#include <iomanip>

using namespace std;

Produtos::Produtos(){

}

Produtos::Produtos(int codigo, string tipo, string nome, float preco, int quantidade){
	this->codigo = codigo;
	this->tipo = tipo;
	this->nome = nome;
	this->preco = preco;
	this->quantidade = quantidade;

	Produtos::guardar_produto(codigo, tipo, nome, preco, quantidade);
}

Produtos::Produtos(string tipo, string nome, float preco, int quantidade){

	fstream contadorcodigo;
	int codigo_contador;
	int codigo_final;
	contadorcodigo.open("./db/etc/contadorcodigo.txt");
	if (contadorcodigo.is_open()){
		while (contadorcodigo >> codigo_contador){

		}
		codigo_final = codigo_contador +1;
		ofstream atualizarcontador;
		atualizarcontador.open("./db/etc/contadorcodigo.txt");
		if(atualizarcontador.is_open()){
			atualizarcontador << codigo_final;
		}
	}

	this->codigo = codigo_contador;
	this->tipo = tipo;
	this->nome = nome;
	this->preco = preco;
	this->quantidade = quantidade;

	Produtos::guardar_produto(codigo, tipo, nome, preco, quantidade);
}

Produtos::~Produtos(){

}

void Produtos::set_codigo(int codigo){
	this->codigo = codigo;
}

int Produtos::get_codigo(){
	return codigo;
}

void Produtos::set_nome(string nome){
	this->nome = nome;
}

void Produtos::set_preco(float preco){
	this->preco = preco;
}

void Produtos::set_quantidade(int quantidade){
	this->quantidade = quantidade;
}

void Produtos::set_tipo(string tipo){
	this->tipo = tipo;
}

string Produtos::get_nome(){
	return nome;
} 

float Produtos::get_preco(){
	return preco;
}

int Produtos::get_quantidade(){
	return quantidade;
}

string Produtos::get_tipo(){
	return tipo;
}

void Produtos::guardar_produto(int codigo, string tipo, string nome, float preco, int quantidade){
	ofstream produto;
	string end = ".//db//produtos//";
	end.append(to_string(codigo));
	end.append(".txt");
	produto.open(end);

	if(produto.is_open()){
		produto << codigo << endl;
    	produto << tipo << endl;
    	produto << nome << endl;
    	produto << fixed << setprecision(2) << preco << endl;
    	produto << quantidade << endl;
    	produto.close();
  }


}