#include <iostream>
#include <fstream>
#include <string>

#include "venda.hpp"
#include "cliente.hpp"
#include "analizaprodutos.hpp"
#include "novoproduto.hpp"

using namespace std;

int main(){

	vector<Produtos> sacola;
	int escolha = -1;

	system("clear");
	cout << "/------------------------------------------------------------------------------/" << endl;
	cout << "\n";
	cout << "\t \t \t \t      MENU  \t \t \t";
	cout << "\n\n";
	cout << "/------------------------------------------------------------------------------/" << endl;
	cout << "\n";
	cout << "\tMenu de opções:\n";
	cout << "\t(1) - Modo Venda\n";
	cout << "\t(2) - Modo Estoque\n";
	cout << "\t(3) - Modo Recomendação\n";
	cout << "\t(0) - Sair\n";
	cout << "\tSua Escolha: ";
	
	cin >> escolha;
	
	cout << "\n \n \n \n";
	
	switch(escolha){
		
		case 0:
		system("clear");
			return 0;
			break;

		case 1:{
		
		string cpf;
		cout << "Digite o CPF do cliente: ";
		cin >> cpf;

			string end_cpf = cpf;
			end_cpf += ".txt";
			string end_clientes = "./db/clientes/";
			end_clientes += end_cpf;

			ifstream arq_cliente;
			arq_cliente.open(end_clientes);
			if (arq_cliente.is_open()){
				
				char temp_socio;
				string temp_nome;
				string temp_cpf;
				string texto;
				
				for(int linha = 1; getline(arq_cliente, texto) && linha <=3; linha++){
					if(linha == 1) temp_socio = texto[0];
					if(linha == 2) temp_nome = texto;
					if(linha == 3) temp_cpf = texto;
				}

				if (temp_socio == 'S'){
					cout << "Esse cliente é sócio!" << endl;
					
					
					Cliente* cliente = new Cliente(temp_socio, temp_nome, temp_cpf);

					cout << "Pressione qualquer tecla para continuar " << endl;
					getchar(); getchar();

					Venda venda(cliente);
				}

				else if(temp_socio == 'N'){

					cout << "Esse cliente não é sócio" << endl;
					cout << "Cliente, deseja se tornar sócio agora? (S/n): ";
					char e;
					cin >> e;
					if(toupper(e) == 'S'){
						temp_socio = 'S';
					} else if (toupper(e) == 'N'){
						temp_socio = 'N';
					}

					Cliente* cliente = new Cliente(temp_socio, temp_nome, temp_cpf);

					Venda venda(cliente);
				}
			}

			else{

				system("clear");

				string temp_nome;	
				char temp_socio;

				cout << "/------------------------------------------------------------------------------/" << endl;
				cout << "\t\t\tCadastro de novo Usuário\t\t\t" << endl;
				cout << "/------------------------------------------------------------------------------/" << endl;
				cout << "Digite o nome do novo cliente: ";
				
				getline(cin >> ws, temp_nome);
				cin.clear();
				cout << "Esse Cliente deseja se tornar sócio? (S/n): ";
		  		char escolha;
		  		cin >> escolha;
		  			if (toupper(escolha) == 'S'){
		  				temp_socio = 'S';
					cout << "Este cliente agora é um sócio!" << endl;
					cout << "Pressione qualquer tecla para continuar " << endl;
					getchar(); getchar();

		  			} 
					else if(toupper(escolha) == 'N'){
		  				temp_socio = 'N';
					cout << "Este cliente não é um sócio." << endl;
					cout << "Pressione qualquer tecla para continuar " << endl;
					getchar(); getchar();
		  			}


				Cliente* cliente = new Cliente(temp_socio, temp_nome, cpf);
				Venda venda(cliente);
			}

			break;
		}
		case 2:{

			system("clear");

			int esc = 0;
			cout << "Escolha: " << endl;
			cout << "(1) Adicionar novo produto\n(2) Editar estoque de produto existente" << endl;
			cin >> esc;
			if(esc == 1){
				NovoProduto novo;
			}
			else if (esc == 2){
				AnalizaProdutos atualizar;
				atualizar.atualizarQuantidade();
			}
			break;
		}
		case 3:{
			system("clear");

			cout << "/------------------------------------------------------------------------------/" << endl;
			cout << "\t\t\t Recomendações \t\t\t" << endl;
			cout << "/------------------------------------------------------------------------------/" << endl;
			break;
		}
		
	default:
		system("clear");
		
		cout << "opção errada, digite novamente:\n";
		cin >> escolha;
		break;
	}

}
