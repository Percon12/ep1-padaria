#include "venda.hpp"
#include "analizaprodutos.hpp"
#include "cliente.hpp"
#include "produtos.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}


vector<Produtos> sacola;

Venda::Venda(Cliente * cliente){
	system("clear");

	cout << "/------------------------------------------------------------------------------/" << endl;
	cout << "\n";
	cout << "\t \t \t \t      VENDA  \t \t \t";
	cout << "\n";
	cout << "/------------------------------------------------------------------------------/" << endl;
	cout << "\n \n";

	AnalizaProdutos vector;

	vector.mostraProdutos();
	add_produto();
	salvarcompras(cliente);

}

void Venda::add_produto(){
	cin.clear();

	string temp_tipo;
	string temp_nome;
	float temp_preco;
	int temp_quant;
	string text;
	cout << "\nDigite o codigo do produto que o cliente deseja comprar: ";

	int temp_codigo = getInput<int>();
	cout << "Quantidade desse produto que o cliente deseja comprar: ";

	int compra_quantidade = getInput<int>();
	int quantidade_estoque = 0;


	fstream arquivo;
	string fileText;
	string endcodigo = to_string(temp_codigo);
	string endereco = "./db/produtos/";
	endereco.append(endcodigo);
	endereco.append(".txt");
	
	arquivo.open(endereco);
	if(arquivo.is_open()){

		for(int lines = 1; getline(arquivo, fileText) && lines <=5; lines++){
			if(lines == 1) temp_codigo = stoi(fileText);
			if(lines == 2) temp_tipo = fileText;
			if(lines == 3) temp_nome = fileText;
			if(lines == 4) temp_preco = stof(fileText);
			if(lines == 5) temp_quant = stoi(fileText);
		}
		quantidade_estoque = temp_quant;
		temp_quant = compra_quantidade;

		this->sacola.push_back(new Produtos(temp_codigo, temp_tipo, temp_nome, temp_preco, temp_quant));
		
	}

	cout << "\nProduto adicionado à sacola com sucesso!\n\nDeseja adicionar outro produto (S/N)?:" << endl;
	cin.clear();
	char escolha;
	cin >> escolha;
	if (toupper(escolha) == 'S'){
		add_produto();
	}
	else if(toupper(escolha) == 'N'){
		totaldacompra();
	}


	temp_quant = quantidade_estoque - compra_quantidade;
	this->sacola.push_back(new Produtos(temp_codigo, temp_tipo, temp_nome, temp_preco, temp_quant));
}

void Venda::totaldacompra(){
	system("clear");
	cout << "Produtos na sacola:\n\n";
	cout << "Nome\tQuantidade Preco\n";
	for (unsigned int i = 0; i < sacola.size(); i++){
		cout << this->sacola[i]-> get_nome() << "\t" << this->sacola[i]->get_quantidade() << "\t   " << this->sacola[i]->get_preco() << endl;
	}

	float total = 0;
	float calculo = 0;
	for (unsigned int i = 0; i < this->sacola.size(); i++){
		calculo = this->sacola[i]->get_preco() * this->sacola[i]->get_quantidade();
		total += calculo;
	}

	/*if (Cliente *cliente -> temp_socio == "s")){
		total = total - (total * 0.15);
	}*/
	
	cout << "\nO total da compra é R$: \n";
	cout << total;

	cout << "\nDeseja efetuar a compra de todos os produtos da sacola (S/N)?" << endl;
	
	char r;
	cin >> r;
		if(toupper(r) == 'S'){
			cout << "Compra finalizada com sucesso!" << endl;
		}
		else{
				add_produto();
			}		
}
void Venda::salvarcompras(Cliente* cliente){
	cliente->escrever_dados();
}