#ifndef ANALIZAPRODUTOS_HPP
#define ANALIZAPRODUTOS_HPP
#include "produtos.hpp"
#include <vector>

using namespace std;

class AnalizaProdutos{

public:
	AnalizaProdutos();
	~AnalizaProdutos();
	void mostraProdutos();
	void atualizarQuantidade();
	vector<Produtos> listaprodutos;

};

#endif