#ifndef PRODUTOS_HPP
#define PRODUTOS_HPP

#include <string>

using namespace std;

class Produtos{

private:
	int codigo;
	string nome;
	float preco;
	int quantidade;
	string tipo;


public:
	Produtos();
	Produtos(int codigo, string tipo, string nome, float preco, int quantidade);
	Produtos(string tipo, string nome, float preco, int quantidade);
	~Produtos();

	void set_codigo(int codigo);
	int get_codigo();

	void set_nome(std::string nome);
	string get_nome();

	void set_preco(float preco);
	float get_preco();

	void set_quantidade(int quantidade);
	int get_quantidade();

	void set_tipo(string tipo);
	string get_tipo();

	void guardar_produto(int codigo, string tipo, string nome, float preco, int quantidade);

};

#endif